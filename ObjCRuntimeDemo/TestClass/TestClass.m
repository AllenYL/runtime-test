//
//  TestClass.m
//  ObjCRuntimeDemo
//
//  Created by Mr.LuDashi on 2017/1/4.
//  Copyright © 2017年 ZeluLi. All rights reserved.
//

#import "TestClass.h"
#import "RuntimeKit.h"

@interface SecondClass : NSObject
- (void)noThisMethod:(NSString *)value;
@end

@implementation SecondClass
- (void)noThisMethod:(NSString *)value {
    NSLog(@"原类未曾实现对应方法，SecondClass中实现了该方法，参数: %@", value);
}

@end


@interface TestClass(){
    NSInteger _var1;
    int _var2;
    BOOL _var3;
    double _var4;
    float _var5;
}
@property (nonatomic, strong) NSMutableArray *privateProperty1;
@property (nonatomic, strong) NSNumber *privateProperty2;
@property (nonatomic, strong) NSDictionary *privateProperty3;
@end

@implementation TestClass

+ (void)classMethod: (NSString *)value {
    NSLog(@"publicTestMethod1");
}

- (void)publicTestMethod1: (NSString *)value1 Second: (NSString *)value2 {
    NSLog(@"publicTestMethod1");
}

- (void)publicTestMethod2 {
    NSLog(@"publicTestMethod2");
}

- (void)privateTestMethod1 {
    NSLog(@"privateTestMethod1");
}

- (void)privateTestMethod2 {
    NSLog(@"privateTestMethod2");
}

#pragma mark - 方法交换时使用
-(void)method1:(NSString *)string {
    
    NSLog(@"我是Method1的实现, string: %@", string);
}

#pragma mark - 消息转发机制
/**
 
 >>>> 1
 在本类和父类中没有找到SEL的IML实现时会执行下方的方法, 允许给类添加该方法的实现或者其他处理
 */
+ (BOOL)resolveInstanceMethod:(SEL)sel {

    // 处理方式一： 默认返回NO，会接着执行forwordingTargetForSelector:方法，
    return NO;
    
    
    // 处理方式二： 手动添加一个方法来弥补，返回YES，不再执行forwordingTargetForSelector:方法
    [RuntimeKit addMethod:[self class] method:sel method:@selector(dynamicAddMethod:)];
    return YES;
}

- (void)dynamicAddMethod: (NSString *) value {
  
    //运行时方法拦截
    NSLog(@"被调用方法在本类及其父类都未曾实现，为避免crash进行了方法替换，参数：%@", value);
}



/**
 
 >>>> 2
 将当前对象不存在的SEL传给其他存在该SEL的对象， 允许进行消息转发(这个转发是单一的)
 */
- (id)forwardingTargetForSelector:(SEL)aSelector {
    
    // 处理方式一：返回本对象(实际上本类又没有实现该方法，返回也没有意义)，表示最后还是由自己来处理消息
    return self;
    
    // 处理方式二：返回实现了该方法的类对象(这个类是其他一个类)，让别的类对象来处理这条消息
    return [SecondClass new];
}

/*

 >>>> 3
 当forwardingTargetForSelector: 返回对象为self或nil时，
 调用下面这个方法抛出一个方法签名、方法返回的数据类型，交由forwardInvocation: 去处理
 */
- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector {
    
    //查找父类的方法签名
    NSMethodSignature *signature = [super methodSignatureForSelector:selector];
    if(signature == nil) {
        
//        NSString *sel = NSStringFromSelector(selector);
        
        //动态造一个 setter函数
//        signature = [NSMethodSignature signatureWithObjCTypes:"v@:@"];
        
        //动态造一个 getter函数
        signature = [NSMethodSignature signatureWithObjCTypes:"@@:"];
    }
    return signature;
}

/*
 
 >>>> 4
 * 当 methodSignatureForSelector: 方法返回nil时，执行第5步,
   否则执行下面这个方法，进行最后机会的消息转发，可以多次转发给多个对象去寻找IML
 */
- (void)forwardInvocation:(NSInvocation *)invocation {
    
    NSString *tar = NSStringFromClass([invocation.target class]);
    NSString *key = NSStringFromSelector([invocation selector]);
    NSLog(@"4.target: %@, sel: %@", tar,  key);
}


/*
 >>>> 5 抛出异常，苹果不建议重写，无论如何都应该抛出异常
 */
//- (void)doesNotRecognizeSelector:(SEL)aSelector

@end
