//
//  Student.m
//  ObjCRuntimeDemo
//
//  Created by 公爵 on 2017/3/4.
//  Copyright © 2017年 ZeluLi. All rights reserved.
//

#import "Student.h"
#import <objc/message.h>
void dynamicMethodIMP(id self, SEL _cmd) {
   
    NSLog(@"Sutent类动态添加的方法");
}

@implementation Student

+(BOOL)resolveInstanceMethod:(SEL)sel {
    
    if (sel == @selector(study)) {
        class_addMethod([self class], sel, (IMP)dynamicMethodIMP, "v@:");
        return YES;
    }
    
    return [super resolveInstanceMethod:sel];
}

@end
